import { createApp, h } from 'vue'
import './style.scss'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'
import 'assets/js/main.js'
import App from './App.vue'
import router from './routes';


const createNewapp = () => {
    const app = createApp({
        render: () => h(App)
    });
    app.use(router);
    app.mount('#app')
};

const initApp = async () => {
    createNewapp()
}

initApp().then((d) => {
    // initialized
})


