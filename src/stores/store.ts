import { defineStore } from 'pinia';

export const authStore = defineStore('authStore', () => {
    let token = null;

    const setToken = (data: object) => {
        token = data?.token;
    }

    return{
        token,
        setToken,
    }
})