import Joi from "joi";
import { getErrors } from "../helpers/hideError";
import { Ilogin, IloginErrors } from "../types/types";

const schema = Joi.object({
    username: Joi.string()
        .max(20)
        .label('Username')
        .required()
        .messages({
            'string.empty': '{{#label}} is required'
        }),

    password: Joi.string()
        .label('Password')
        .required()
        .messages({
            'string.empty': '{{#label}} is required'
        }),

}).options({
    abortEarly: false,
})

export const loginValidation = (data: Ilogin, errorObj: IloginErrors) => {
    const result = schema.validate(data);
    if (result.error) {
        getErrors(result, errorObj);
        return { passed: false };
    }
    return { passed: true };
}

