import Joi from "joi";
import { getErrors } from "../helpers/hideError";
import { IRegister, IregisterErrors } from "../types/types";

const schema = Joi.object({
    username: Joi.string()
        .min(15)
        .max(20)
        .label('Username')
        .required()
        .messages({
            'string.empty': '{{#label}} is required'
        }),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
        .label('E-mail')
        .required()
        .messages({
            'string.empty': '{{#label}} is required'
        }),

    password: Joi.string()
        .label('Password')
        .required()
        .messages({
            'string.empty': '{{#label}} is required'
        }),

}).options({
    abortEarly: false,
})

export const registerValidation = (data: IRegister, errorObj: IregisterErrors) => {
    const result = schema.validate(data);
    if (result.error) {
        getErrors(result, errorObj);
        return { passed: false };
    }
    return { passed: true };
}

