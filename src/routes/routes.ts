
const Register = () => import('views/pages/register.vue');
const Login = () => import('views/pages/login.vue');
const Home = () => import('views/pages/home.vue');
const Blank = () => import('views/pages/blank.vue');
const Chat = () => import('views/pages/chat.vue');
const notFound = () => import('views/pages/404.vue');

const routes = [
    {
        path: '/login',
        component: Login,
        name: 'Login',
        meta: {
            title: 'Login',
            //   icon: HomeIcon,
            color: 'text-indigo-410',
            requiresAuth: false,
            //   parentPath: 'Home'
        },
    },
    {
        path: '/',
        redirect:'/login',
    },
    {
        path: '/register',
        component: Register,
        name: 'Register',
        meta: {
            title: 'Register',
            //   icon: HomeIcon,
            color: 'text-indigo-410',
            requiresAuth: false,
            //   parentPath: 'Home'
        },
    },
    {
        path: '/home',
        component: Home,
        name: 'Home',
        meta: {
            title: 'Home',
            color: 'text-indigo-410',
            requiresAuth: true,
        },
    },
    {
        path: '/blank',
        component: Blank,
        name: 'Blank',
        meta: {
            title: 'Blank',
            color: 'text-indigo-410',
            requiresAuth: true,
        },
    },
    {
        path: '/chat',
        component: Chat,
        name: 'Chat',
        meta: {
            title: 'Chat',
            color: 'text-indigo-410',
            requiresAuth: true,
        },
    },
    {
        path: '/:pathMatch(.*)*',
        component: notFound
    },


];

export default routes;
