import { createRouter, createWebHistory, Router } from 'vue-router'

import routes from './routes'

const router: Router = createRouter({
    history: createWebHistory(),
    routes,
    linkActiveClass: "active",
    linkExactActiveClass: "exact-active",
})

export default router;

