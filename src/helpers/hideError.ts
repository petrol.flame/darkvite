import { Ivalidation } from "../types/types";

export const ridErrors = (err: any) => {
    setTimeout(() => {
        for (const key in err) {
            if (Object.prototype.hasOwnProperty.call(err, key)) {
                err[key as string] = undefined;
            }
        }
    }, 3000);
}

export const getErrors = (result: Ivalidation, errorObj: any) => {
    result.error.details.forEach((e) => {
        errorObj[e.context?.key as string] = e.message.replace(/"/g, ``) + "!";
    });
    ridErrors(errorObj);
}

