import Joi from "joi";

export interface Ilogin {
    username: string | undefined,
    password: string | undefined,
}

export interface IloginErrors {
    [index : string] : any,
    username: string | undefined,
    password: string | undefined,
}

export interface IregisterErrors {
    [index : string] : any,
    username: string | undefined,
    email: string | undefined,
    password: string | undefined,
}

export interface IRegister {
    username: string | undefined,
    email: string | undefined,
    password: string | undefined,
}

export interface Ivalidation {
    error: Joi.ValidationError;
    warning?: Joi.ValidationError | undefined;
    value: undefined;
}
