import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import * as path from 'path';

const resolvePath = (dir: string) => {
  return path.resolve(__dirname, 'src', dir)
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  // root: path.resolve(__dirname, 'src'),
  resolve: {
    alias: {
      views:resolvePath('views'),
      components:resolvePath('components'),
      assets:resolvePath('assets'),
    }
  },
  // build: {
  //   rollupOptions: {
  //     output:{
  //       globals:{
  //         jQuery: 'window.jQuery',
  //         $: 'window.$'
  //       }
  //     }
  //   }
  // }
})
